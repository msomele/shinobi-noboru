﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BotonAscensor : MonoBehaviour {
    public GameObject ascensor;
    public GameObject puerta;
    public GameObject suelo;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}


    private void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Player")
        {
            ascensor.SetActive(true);
            Destroy(puerta);
            Destroy(suelo);
        }
    }
}
