﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Puerta : MonoBehaviour {

    public GameObject puerta;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag == "Player" && Inventory.llave_2)
        {
            if (puerta.tag == "Puerta_2")
            {
                Destroy(puerta);
            }
        }

        if (col.gameObject.tag == "Player" && Inventory.llave_3)
        {
            if (puerta.tag == "Puerta_3")
            {
                Destroy(puerta);
            }
        }

        if (col.gameObject.tag == "Player" && Inventory.llave_6)
        {
            if (puerta.tag == "Puerta_6")
            {
                Destroy(puerta);
            }
        }

        if (col.gameObject.tag == "Player" && Inventory.llave_X)
        {
            if (puerta.tag == "Puerta_X")
            {
                Destroy(puerta);
            }
        }
		if (col.gameObject.tag == "Player" && Inventory.llave_3) {
			if (puerta.tag == "Puerta_3") {
				Destroy (puerta);
			}
		}
    }
}
