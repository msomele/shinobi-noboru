﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckGroundedEnemy : MonoBehaviour
{

    public static bool groundedEnemy;


    // Use this for initialization
    void Start()
    {
        groundedEnemy = true;
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerStay(Collider col)
    {
        if (col.gameObject.tag == ("Suelo"))
        {
            groundedEnemy = true;

        }
    }


    private void OnTriggerExit(Collider col)
    {
        if (col.gameObject.tag == "Suelo")
        {
            groundedEnemy = false;
        }

    }
}

