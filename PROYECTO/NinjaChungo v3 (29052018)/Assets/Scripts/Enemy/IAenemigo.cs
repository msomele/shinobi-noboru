﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IAenemigo : MonoBehaviour
{
    public float visionRadious;
    public float speed;
    public GameObject player;
    public GameObject zombie;
    private Vector3 InitialPosition;
    Animator anim;
    public Transform Ninja;
    public float dirNum;
    public Rigidbody rb;
    public float empuje;
    public bool isGrounded;


    // Use this for initialization
    void Start()
    {
        InitialPosition = transform.position;
        anim = GetComponent<Animator>();
        
    }

    // Update is called once per frame
    void Update()
    {
        isGrounded = CheckGroundedEnemy.groundedEnemy;
        Vector3 target = InitialPosition;
        float Dist = Vector3.Distance(player.transform.position, transform.position);
        if (Dist < visionRadious)
        {
            target = player.transform.position;
            anim.SetBool("andar", true);
            float fixedSpeed = speed * Time.deltaTime;
            transform.position = Vector3.MoveTowards(transform.position, target, fixedSpeed);
        }
        else
        {
            anim.SetBool("andar", false);
        }
        Vector3 Heading = Ninja.position - transform.position;
        dirNum = AngleDir(transform.forward, Heading, transform.up);
        if (dirNum > 0)
        {
            transform.localScale = new Vector3(0.45f, 0.45f, 0.45f);
        }
        else
        {
            transform.localScale = new Vector3(0.45f, 0.45f, -0.45f);
        }
    }
    float AngleDir(Vector3 fwd, Vector3 targetDir, Vector3 up)
    {
        Vector3 perp = Vector3.Cross(fwd, targetDir);
        float dir = Vector3.Dot(perp, up);
        if(dir > 0)
        {
            return 1f;
        } else if(dir < 0f)
        {
            return -1f;
        } else
        {
            return 0f;
        }
    }
    private void OnTriggerEnter(Collider col)
    {
        if (!isGrounded)
        {
            rb.AddForce(0, -empuje, 0, ForceMode.Impulse);
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "Abismo")
        {
            Destroy(this.gameObject);
        }
    }
}
        
    
      

        
  



