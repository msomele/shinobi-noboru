﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PinchosMuerte : MonoBehaviour {
    public GameObject Zombie;

    private void OnCollisionEnter(Collision col)
    {
		if (col.gameObject.tag == "Zombie")
        {
            
            Destroy(Zombie.gameObject);
            
        }

    }
}
