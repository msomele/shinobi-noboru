﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class BarraCarga : MonoBehaviour {
	public float speed;
	public Scrollbar Barra;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		Barra.size = speed;
		speed += 0.005f;
		if (speed >= 1) {
			SceneManager.LoadScene ("nivel2");
		}
	}
}
