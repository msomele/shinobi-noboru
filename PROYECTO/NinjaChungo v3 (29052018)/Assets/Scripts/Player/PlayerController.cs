﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class PlayerController : MonoBehaviour
{

    public float velocity;
	public float velTumb;
    public Animator Anim;
    public float altura;
    public float alturaDobleSalto;
    bool dobleSalto;
    public bool isGrounded;
    public bool derecha;
    bool tumbado;
    float velAux;
	public float contadorShuriken;
	public float contadorPergamino = 0;
	public GameObject generadorShuriken;
    public Collider colTumbado;
    public Collider colDePie;
    public Collider ColPared;
    public Collider ColTecho;
    public Rigidbody rb;
    public float empuje;
    private CheckPared checkPared;
	public bool SuriIs;
	private SaltoPared saltoPared;
	private GameObject surilan;
	public bool Disparando = false;


    // Use this for initialization
    void Start()
    {
		saltoPared = GetComponentInChildren<SaltoPared>();
        checkPared = GetComponentInChildren<CheckPared>();

        if (checkPared == null)
        {
            enabled = false;
            Debug.Log("No se encuentra checkPared");
            return;
        }
       velAux = velocity;
    }

    // Update is called once per frame
    void Update()
	{
		
		isGrounded = CheckGrounded.grounded;
		Caer ();
      
		if (Input.GetKey("d") && checkPared.paredDer == false && (!saltoPared.AirDer && !saltoPared.AirIzq))
        {
          
            transform.position = transform.position + new Vector3(velocity, 0, 0);
            transform.rotation = Quaternion.Euler(0, -260, 0);
            Anim.SetBool("Correr", true);
            tumbarse();
            salto(1);
            derecha = true;
            
        }
		else if (Input.GetKey("a") && checkPared.paredIzq == false && (!saltoPared.AirDer && !saltoPared.AirIzq))
        {
            transform.position = transform.position + new Vector3(-velocity, 0, 0);
            transform.rotation = Quaternion.Euler(0, 260, 0);
            Anim.SetBool("Correr", true);
            tumbarse();
            salto(-1);
            derecha = false;

        } else{
            Anim.SetBool("Correr", false);
            tumbarse();
            salto(0);

			if (Input.GetKeyDown (KeyCode.Mouse0) && isGrounded && !GameObject.FindGameObjectWithTag ("ShurikenLanzado") && contadorShuriken > 0) {
				Anim.SetTrigger ("Ataque");
				disparoShuriken ();
			}
			if (Input.GetKeyDown ("f") && SuriIs == true && contadorPergamino == 2) {
				teleportShuriken ();
			}
        }




      
        

	}
	void FixedUpdate(){
		rb.AddForce (0f, -9.8f, 0f);
	}
	void Caer(){
		if (!isGrounded) {
			Anim.SetBool ("Cayendo", true);
           
		} else {
			Anim.SetBool ("Cayendo", false);
		}
	}

    void tumbarse()
    {
       
        if (Input.GetKey(KeyCode.S) && (isGrounded))
        {
            colTumbado.enabled=true;
            colDePie.enabled =false;
            
            tumbado = true;
            Anim.SetBool("Tumbarse", true);

            if (Input.GetKey(KeyCode.A)|| Input.GetKey(KeyCode.D))
            {
                velocity = velTumb;
                Anim.SetBool("MovimientoTumb", true);
            }
            else
            {
                Anim.SetBool("MovimientoTumb", false);
            }
        }
        if(Input.GetKeyUp(KeyCode.S) && CheckTecho.levanta)
        {
            Anim.SetBool("Tumbarse", false);
            Anim.SetBool("MovimientoTumb", false);
            colDePie.enabled = true;
            colTumbado.enabled = false;
            
            tumbado = false;
            velocity = velAux;
        } 
    }


    void salto(float salto)
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (isGrounded && !tumbado)
            {
                if (salto != 0)
                {
                    Anim.SetTrigger("SaltoCorriendo");
                    GetComponent<Rigidbody>().velocity = new Vector3(salto, altura, 0);
                    dobleSalto = true;

                }
                else
                {
                    Anim.SetTrigger("SaltoEstatico");
                    GetComponent<Rigidbody>().velocity = new Vector2(salto, altura);
                    dobleSalto = true;

                }

            }
            else if (dobleSalto)
            {
                Anim.SetTrigger("DobleSalto");
                GetComponent<Rigidbody>().velocity = new Vector3(0, alturaDobleSalto, 0);
                dobleSalto = false;
            }
        }
    }

    private void OnTriggerEnter(Collider col)
    {
        if ((col.gameObject.tag == ("Shuriken")) && (contadorShuriken < 3))
        {

            Destroy(col.gameObject);
            contadorShuriken++;

            
        }
		if (col.gameObject.tag == ("Pergamino1")){
			Destroy (col.gameObject);
			contadorPergamino++;

		}
		if (col.gameObject.tag == ("Pergamino2")){
			Destroy (col.gameObject);
			contadorPergamino++;

		}

        if (col.gameObject.tag == ("Pared") && derecha){
            
            checkPared.paredDer = true;
        } else if(col.gameObject.tag == ("Pared") && !derecha)
        {
            checkPared.paredIzq = true;
        } else
        {
            checkPared.paredDer = false;
            checkPared.paredIzq = false;
        }
       
    }
    void OnTriggerExit(Collider col)
    {
        if (col.gameObject.tag == "Pared")
        {
            checkPared.paredIzq = false;
            checkPared.paredDer = false;
        }

    }


    public void disparoShuriken(){
		if ((Input.GetKeyDown (KeyCode.Mouse0) && (contadorShuriken > 0) && (!GameObject.FindGameObjectWithTag ("ShurikenLanzado")))) {
			//Spawner.IsshurikenOnScreen = true;
			SuriIs = true;
			generadorShuriken.GetComponent<Spawner> ().Fire ();
			contadorShuriken--;
			Disparando = true;
		} else {
			Disparando = false;
		}
	}
	void teleportShuriken(){
		surilan = GameObject.FindGameObjectWithTag ("ShurikenLanzado");
		if (GameObject.FindGameObjectWithTag ("ShurikenLanzado")) {
			transform.position = new Vector3 (surilan.transform.position.x, surilan.transform.position.y, surilan.transform.position.z);
			Destroy (surilan.gameObject);
			SuriIs = false;
		}
	}







}