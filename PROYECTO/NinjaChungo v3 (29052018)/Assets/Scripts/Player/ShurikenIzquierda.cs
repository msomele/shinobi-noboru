﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShurikenIzquierda : MonoBehaviour {
	public GameObject shuriken;
	public static float speedShuriken= -0.15f;


	void Update(){

		transform.position += new Vector3 (speedShuriken, 0, 0);

	}

	void OnCollisionEnter(Collision col){
		if (col.gameObject.tag != "Player" && col.gameObject.tag == "Zombie")
		{
			Destroy(this.gameObject);
			gameObject.SetActive (false);
            Destroy(col.gameObject);
			//Spawner.IsshurikenOnScreen = false;
		} else if (col.gameObject.tag == "Pared"){
			gameObject.SetActive (false);
		}
	}
}