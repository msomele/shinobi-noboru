﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class Spawner : MonoBehaviour {
	
	public Transform ShurikenSpawn;
	public GameObject ShurikenDer;
	public GameObject ShurikenIzq;
	public GameObject Ninja;
	//public static bool IsshurikenOnScreen;
    


    public void Fire (){

		if (Ninja.transform.rotation == Quaternion.Euler (0, -260, 0)) {
			Instantiate (ShurikenDer, ShurikenSpawn.transform.position, ShurikenDer.transform.rotation);
			//Spawner.IsshurikenOnScreen = true;
		}
		if (Ninja.transform.rotation == Quaternion.Euler (0, 260, 0)) {
			Instantiate (ShurikenIzq, ShurikenSpawn.transform.position, ShurikenIzq.transform.rotation);
			//Spawner.IsshurikenOnScreen = true;
		}

    }

}
