﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaltoPared : MonoBehaviour {
    private CheckPared checkPared;
	private PlayerController playerController;
	public GameObject jugador;
    public bool isGrounded;
    public Rigidbody rb;
    public float salto;
    public float altura;
	public bool AirIzq;
	public bool AirDer;
	public Animator Anim;
    // Use this for initialization
    void Start () {
        checkPared = GetComponentInChildren<CheckPared>();
		playerController = GetComponentInChildren<PlayerController> ();
	}
	
	// Update is called once per frame
	void Update () {
		isGrounded = CheckGrounded.grounded;
		if (playerController.contadorPergamino >= 1) {
			if (checkPared.paredDer && !isGrounded && Input.GetKeyDown (KeyCode.Space) && AirDer == true) {
				transform.rotation = Quaternion.Euler (0, 260, 0);
				GetComponent<Rigidbody> ().velocity = new Vector3 (salto, altura, 0);
				AirIzq = false;
				jugador.GetComponent<PlayerController> ().derecha = false;

			} 
			if (checkPared.paredIzq && !isGrounded && Input.GetKeyDown (KeyCode.Space) && AirIzq == true) {
				transform.rotation = Quaternion.Euler (0, -260, 0);
				GetComponent<Rigidbody> ().velocity = new Vector3 (-salto, altura, 0);
				AirDer = false;
				jugador.GetComponent<PlayerController> ().derecha = true;

			}
		}
	}
	private void OnTriggerStay(Collider Col)
	{
		if (Col.gameObject.tag == "Pared" && !isGrounded)
		{
			AirIzq = true;
			AirDer = true;
		}
		else  {
			AirIzq = false;
			AirDer = false; 
		}
	}
}
