﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour {
    
    public static bool llave_1;
    public static bool llave_2;
    public static bool llave_3;
    public static bool llave_4;
    public static bool llave_5;
    public static bool llave_6;
    public static bool llave_X;

    void Start () {
        llave_1 = false;
        llave_2 = false;
        llave_3 = false;
        llave_4 = false;
        llave_5 = false;
        llave_6 = false;
        llave_X = false;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    
    private void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "llave_1")
        {
            Destroy(col.gameObject);

            llave_1 = true;
        }
        if (col.gameObject.tag == "llave_2")
        {
            Destroy(col.gameObject);

            llave_2 = true;
        }
        if (col.gameObject.tag == "llave_3")
        {
            Destroy(col.gameObject);

            llave_3 = true;
        }
        if (col.gameObject.tag == "llave_4")
        {
            Destroy(col.gameObject);

            llave_4 = true;
        }
        if (col.gameObject.tag == "llave_5")
        {
            Destroy(col.gameObject);

            llave_5 = true;
        }
        if (col.gameObject.tag == "llave_6")
        {
            Destroy(col.gameObject);

            llave_6 = true;
        }
        if (col.gameObject.tag == "llave_X")
        {
            Destroy(col.gameObject);

            llave_X = true;
        }
    }
}
