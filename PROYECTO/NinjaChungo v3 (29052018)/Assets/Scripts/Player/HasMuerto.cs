﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HasMuerto : MonoBehaviour {
    public GameObject respawn;
    public GameObject Personaje;
	public Rigidbody rb;
	public GameObject Plata; 


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKey(KeyCode.Mouse0))
        {
            Personaje.SetActive(true);
            Personaje.transform.position = respawn.transform.position;
			rb.velocity = new Vector2 (0, 0);
            this.gameObject.SetActive(false);
			if (!GameObject.FindGameObjectWithTag ("SueloRompible")) {
				Instantiate (Plata, Plata.transform.position, Plata.transform.rotation);
			}
        }

    }
}
