﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckGrounded : MonoBehaviour {

    public static bool grounded;
    

	// Use this for initialization
	void Start () {
        grounded = true;
	}
	
	// Update is called once per frame
	void Update () {
        
	}

    private void OnTriggerStay(Collider col)
    {
        if (col.gameObject.tag == ("Suelo")){
            grounded = true;
            
        }
		if (col.gameObject.tag == ("SueloRompible")){
			grounded = true;

		}
    }


    private void OnTriggerExit(Collider col)
    {
        if (col.gameObject.tag == "Suelo")
        {
            grounded = false;
        }
		if (col.gameObject.tag == ("SueloRompible")){
			grounded = false;

		}
       
    }
}
