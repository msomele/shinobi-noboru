﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckPared : MonoBehaviour
{

    public bool paredIzq;
    public bool paredDer;

    // Use this for initialization
    void Start()
    {
        paredIzq = false;
        paredDer = false;
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerStay(Collider col)
    {
        if (col.gameObject.tag == "Pared" || col.gameObject.tag == "Pared_Rompible")
        {
            paredIzq = true;
            paredDer = true;
        }
    }


    private void OnTriggerExit(Collider col)
    {
        if (col.gameObject.tag == "Pared" || col.gameObject.tag == "Pared_Rompible")
        {
            paredIzq = false;
            paredDer = false;
        }

    }
}