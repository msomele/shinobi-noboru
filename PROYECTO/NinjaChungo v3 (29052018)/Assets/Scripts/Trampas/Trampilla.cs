﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trampilla : MonoBehaviour {
    public bool pinchosAbajo = true;
    public Animator anim;
    public Collider Puntas;

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Player" && pinchosAbajo)
        {
            Debug.Log("Entra");
            
            anim.SetBool("subir", true); 
            pinchosAbajo = false;
            Puntas.enabled = true;
            
        }
 
        else if  (col.gameObject.tag == "Player" && !pinchosAbajo) { 
        
            Debug.Log("Sale");
            
            anim.SetBool("subir", false);
            pinchosAbajo = true;
            Puntas.enabled = false;
        }
        

    }
}
