﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerBolas : MonoBehaviour {

	public GameObject PrefabToSpawn;
	GameObject ClonePrefab;
	public float speed;
	public float Frequency;
	public float secondsCounter;
	public Rigidbody rb;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		secondsCounter += Time.deltaTime;
		if (secondsCounter >= Frequency) {
			secondsCounter = 0;
			PrefabToSpawn = Instantiate (PrefabToSpawn, transform.position, Quaternion.identity) as GameObject;
		}
		rb.AddForce (0, -speed, 0);
	}
}
