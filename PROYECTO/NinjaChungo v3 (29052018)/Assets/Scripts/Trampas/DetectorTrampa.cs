﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetectorTrampa : MonoBehaviour {
    public GameObject bola;
	public GameObject detector;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter(Collider col)
    {
        
        if (col.gameObject.tag == "Player")
        {
            bola.GetComponent<scriptBola>().lanzarBola();
			Destroy (detector.gameObject);
           
        }
    }
}
