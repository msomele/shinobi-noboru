﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerTrampa_A : MonoBehaviour {

    public Transform FlechaSpawn;
    public GameObject Flecha;
    

    public void Fire()
    {

        Instantiate(Flecha, FlechaSpawn.transform.position, Flecha.transform.rotation);

    }


}
