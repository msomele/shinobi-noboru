﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scriptBola : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        
    }

    public void lanzarBola()
    {
        GetComponent<Rigidbody>().velocity = new Vector3(-40, this.transform.position.y, 0);
    }

    private void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag == "Pared_Rompible")
        {
            Destroy(this.gameObject);
        }
    }
}
