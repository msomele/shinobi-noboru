﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlataformaCae : MonoBehaviour {
	public float contador;
	public float freq;
	public bool contact;
	public Rigidbody rb;
	public Animator Anim;


	// Use this for initialization
	void Start () {
		contact = false;
	}
	
	// Update is called once per frame
	void Update () {
		if (contact == true) {
			contador += Time.deltaTime;
		}
		if (contador >= freq) {
			contador = 0;
			rb.constraints = RigidbodyConstraints.None;
		
		}
	}
	void OnCollisionEnter(Collision Col){
		
		if (Col.gameObject.tag == "Player") {
			
			contact = true;
			Anim.SetBool ("tambaleo", true);

		}
		if (Col.gameObject.tag == "Pinchos") {
			gameObject.SetActive (false);
		}

	}
	void OnCollisionExit(Collision Col){
		if (Col.gameObject.tag == "Player") {
			contact = false;
			contador = 0;
			Anim.SetBool ("tambaleo", false);
		}
	}

}
