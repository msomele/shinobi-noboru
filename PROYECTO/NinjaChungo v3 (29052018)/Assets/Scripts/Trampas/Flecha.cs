﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flecha : MonoBehaviour {

    public GameObject flecha;
    public static float speedFlecha = 0.5f;


    void Update()
    {
		Destroy(this.gameObject, 0.8f);
        transform.position += new Vector3(speedFlecha, 0, 0);

    }

    void OnCollisionEnter(Collision col)
	{

		if (col.gameObject.tag == "Player") {
			Destroy (this.gameObject);
		}
	}
}